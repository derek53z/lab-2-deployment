from marshmallow import Schema, fields, validate

# Nested schema
class TechnicalDetailsSchema(Schema):
    processor = fields.String(required=True, validate=validate.Length(min=1))
    ram  = fields.String(required=True, validate=validate.Length(min=1))
    storage = fields.String(required=True, validate=validate.Length(min=1))
    operating_system = fields.String(required=True, validate=validate.Length(min=1)) 

# Base schema
class DeviceSchema(Schema):
    name = fields.String(required=True, validate=validate.Length(min=1))
    type = fields.String(required=True, validate=validate.OneOf(["laptop", "desktop"]))
    brand = fields.String(required=True, validate=validate.Length(min=1))
    model = fields.String(required=True, validate=validate.Length(min=1))
    serial_number = fields.String(required=True, validate=validate.Length(min=1))
    state = fields.String(required=True, validate=validate.OneOf(["active", "inactive"]))
    register_date = fields.DateTime(format="%Y-%m-%dT%H:%M:%SZ", required=True)
    location = fields.String(required=True, validate=validate.Length(min=1))
    
    # Another schema nested in this field
    technical_details = fields.Nested(TechnicalDetailsSchema, required=True)