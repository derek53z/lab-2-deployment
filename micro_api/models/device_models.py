import os
from pymongo import MongoClient
from logger.logger_base import log

# This class is in charge of enable database connection
class DeviceModel:
    def __init__(self):
        self.client = None
        self.db = None

    # To connect to the db, this env. variables must be exported frrom the host
    # that runs the app (i.e. your pc).
    def connect_to_database(self):
        mongodb_user = os.environ.get('MONGODB_USER')
        mongodb_pass = os.environ.get('MONGODB_PASS')
        mongodb_host = os.environ.get('MONGODB_HOST')
        
        missing_vars = [var for var, val in locals().items() if val is None]
        if missing_vars:
            missing_vars_str = ', '.join(missing_vars)
            log.critical(f'Variables required but not found: {missing_vars_str}')
            raise ValueError(f'Set environment variables: {missing_vars_str}')
        
        try:
            self.client = MongoClient(
                host=mongodb_host,
                port=27017,
                username=mongodb_user,
                password=mongodb_pass,
                authSource='admin',
                authMechanism='SCRAM-SHA-256',
                serverSelectionTimeoutMS=5000
            )
            self.db = self.client['devices']
            if self.db.list_collection_names():
                log.info('Connected to MongoDB database successfully')
        except Exception as e:
            log.critical(f'Failed to connect to the database: {e}')
            raise

    # This method should only be called if client exists, which happens if the
    # connect_to_database method is successful
    def close_connection(self):
        if self.client:
            self.client.close()